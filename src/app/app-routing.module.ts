import { AuthorizedLayoutComponent } from './modules/layout/authorized-layout/authorized-layout.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: AuthorizedLayoutComponent,
    children: [
      {
        path: 'Form-Builder',
        loadChildren: () =>
          import('./modules/demo-1/demoOne.module').then(m => m.DemoOneModule)
      },
      {
        path: 'Formly-Builder',
        loadChildren: () =>
          import('./modules/demo-2/demoTwo.module').then(m => m.DemoTwoModule)
      },
      {
        path: '**',
        redirectTo: 'Form-Builder/',
        pathMatch: 'full',
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    enableTracing: false,
    onSameUrlNavigation: 'reload'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
