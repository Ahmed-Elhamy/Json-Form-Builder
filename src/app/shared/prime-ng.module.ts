import { MenuModule } from 'primeng/menu';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MenubarModule} from 'primeng/menubar';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,

    //primeNG Modules
    MenuModule,
    MenubarModule,
  ],
  exports: [MenuModule, MenubarModule],
})
export class PrimeNGModule {}
