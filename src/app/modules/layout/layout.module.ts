import { TranslocoRootModule } from './../../transloco/transloco-root.module';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthorizedLayoutComponent } from './authorized-layout/authorized-layout.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';

@NgModule({
  declarations: [
    AuthorizedLayoutComponent,
    NavBarComponent,
  ],
  imports: [CommonModule, RouterModule, TranslocoRootModule],
  // exports: [PrimeNGModule],
})
export class LayoutModule {}
