import { HttpClient } from '@angular/common/http';
import { FormItemModel } from './../../model/formBuilder.model';
import { ChangeDetectorRef, Component, Input, OnChanges, OnInit, Output, SimpleChanges, EventEmitter } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, RequiredValidator, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-form-builder',
  templateUrl: './form-builder.component.html',
  styleUrls: ['./form-builder.component.scss']
})
export class FormBuilderComponent implements OnInit, OnChanges {

  @Input() public FormFields: FormItemModel[];
  @Output() public submitForm: EventEmitter<any> = new EventEmitter;

  public form: FormGroup;
  public _formFields;
  constructor(
    private _formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    private _http: HttpClient
  ) {
    this.form = this._formBuilder.group({});
  }

  ngOnInit(): void {
    this._createDynamicFormGroups(this.FormFields)
  }

  ngOnChanges(changes: SimpleChanges) {
    this.form = this._formBuilder.group({});
    this._snackBar.open('Form Fields have been updated successfully', 'close', {
      duration: 2000,
    })
    this._createDynamicFormGroups(changes.FormFields.currentValue);
}
  private _createDynamicFormGroups(formFields) {
    formFields.forEach(field => {
      this.form.addControl(field.name, this._setValidators(field.validation))
    });
  }

  private _setValidators(validators){
    let validation = new FormControl();
    if (validators){
      if (validators.required) validation.setValidators(Validators.required)
      if (validators.min) validation.setValidators(Validators.minLength(validators.min))
      if (validators.max) validation.setValidators(Validators.maxLength(validators.max))
      if (validators.type == 'email') validation.setValidators(Validators.email)
    }
    return validation;
  }

  public formReset() {
    this.form.reset()
    this._snackBar.open('Form has been reset successfully', 'close', {
      duration: 2000,
    });
  }
  public formSubmit() {
    this.submitForm.emit(this.form.value);
    this._snackBar.open('Form has been submitted successfully', 'close', {
      duration: 2000,
    });
    this.form.reset()
  }



}
