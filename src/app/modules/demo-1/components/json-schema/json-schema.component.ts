import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { fakeFormData } from './../../Data/formData';
import { FormItemModel } from './../../model/formBuilder.model';

@Component({
  selector: 'app-json-schema',
  templateUrl: './json-schema.component.html',
  styleUrls: ['./json-schema.component.scss']
})
export class JsonSchemaComponent implements OnInit {

  constructor(
    private _dialogRef: MatDialogRef<JsonSchemaComponent>,
    @Inject(MAT_DIALOG_DATA) public config: FormItemModel[]
  ) { }

  public JsonSchema: FormItemModel[] = this.config

  ngOnInit(): void {
    console.log(this.config);
    
  }

}
