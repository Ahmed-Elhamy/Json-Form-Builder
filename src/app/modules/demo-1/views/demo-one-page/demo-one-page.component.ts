import { isMobile } from 'src/app/shared/helpers/is-mobile.helper';
import { JsonSchemaComponent } from './../../components/json-schema/json-schema.component';
import { templateData } from './../../Data/templateData';
import { fakeFormData } from './../../Data/formData';
import { FormItemModel } from './../../model/formBuilder.model';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';


@Component({
  selector: 'app-demo-one-page',
  templateUrl: './demo-one-page.component.html',
  styleUrls: ['./demo-one-page.component.scss']
})
export class DemoOnePageComponent implements OnInit {

  constructor(
    private _matDialog : MatDialog
  ) { }

  public JsonValidation:string;
  public jsonObject: any;
  public isMobile = isMobile();
  public formFields: FormItemModel[] = fakeFormData
  public templateFields: FormItemModel[] = templateData
  public aceEditorOptions: any = {
    enableBasicAutocompletion: true,
    printMargin: false
  }
  ngOnInit(): void {
  }


  /**
   * Converts a JavaScript value to a JavaScript Object Notation (JSON) string.
   * @return JavaScript Object Notation (JSON) string.
   */
  get jsonCode () {
    return JSON.stringify(this.formFields, null, 2);
  }

  /**
   * Converts a JavaScript Object Notation (JSON) string into an object.
   * @param v - A valid JSON string.
   * @return JsonValidation - an error if the value is not valid.
   */
  set jsonCode (v) {
    try{
      this.JsonValidation = null;
      this.formFields = JSON.parse(v);
    }
    catch(e) {
      this.JsonValidation = e
    };
  }

  public openJsonSchemaModal(schema: FormItemModel[]) {
    this._matDialog.open(JsonSchemaComponent, {
      data: schema,
      panelClass: ['customDialog']
    })
  }


}
