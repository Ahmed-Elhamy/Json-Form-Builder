import { HttpClient } from '@angular/common/http';
import { templateData } from './../../Data/templateData';
import { JsonSchemaComponent } from './../../components/json-schema/json-schema.component';
import { isMobile } from 'src/app/shared/helpers/is-mobile.helper';
import { surveyFormData } from './../../Data/surveyData';
import { FormItemModel } from './../../model/formBuilder.model';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { sendEmail } from 'src/app/shared/helpers/email-sender';

@Component({
  selector: 'app-survey-page',
  templateUrl: './survey-page.component.html',
  styleUrls: ['./survey-page.component.scss']
})
export class SurveyPageComponent implements OnInit {

  constructor(
    private _matDialog : MatDialog,
    private _http: HttpClient
  ) { }







  public JsonValidation:string;
  public jsonObject: any;
  public formFields: FormItemModel[] = surveyFormData
  public templateFields: FormItemModel[] = templateData
  public isMobile = isMobile();
  public direction = 'horizontal'
  public aceEditorOptions: any = {
    enableBasicAutocompletion: true,
    printMargin: false
  }
  ngOnInit(): void {
  }


  /**
   * Converts a JavaScript value to a JavaScript Object Notation (JSON) string.
   * @return JavaScript Object Notation (JSON) string.
   */
  get jsonCode () {
    return JSON.stringify(this.formFields, null, 2);
  }

  /**
   * Converts a JavaScript Object Notation (JSON) string into an object.
   * @param v - A valid JSON string.
   * @return JsonValidation - an error if the value is not valid.
   */
  set jsonCode (v) {
    try{
      this.JsonValidation = null;
      this.formFields = JSON.parse(v);
    }
    catch(e) {
      this.JsonValidation = e
    };
  }


  public openJsonSchemaModal(schema: FormItemModel[]) {
    this._matDialog.open(JsonSchemaComponent, {
      data: schema,
      panelClass: ['customDialog']
    })
  }

  public emailForm(formData){
    // sendEmail(this._http, formData)
  }


}
