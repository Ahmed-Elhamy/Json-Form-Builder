import { FormlyFieldConfig } from "@ngx-formly/core";

export const formlyFakeFormData =
[
  {
    "key": "Input",
    "type": "input",
    "templateOptions": {
      "label": "Input",
      "placeholder": "Placeholder",
      "description": "Description",
      "required": true,
      "focus": false,
      "disabled": false
    },
    "id": "formly_6_input_Input_0",
    "hooks": {},
    "modelOptions": {},
    "wrappers": [
      "form-field"
    ],
    "_keyPath": {
      "key": "Input",
      "path": [
        "Input"
      ]
    }
  },
  {
    "key": "Textarea",
    "type": "textarea",
    "templateOptions": {
      "label": "Textarea",
      "placeholder": "Placeholder",
      "description": "Description",
      "required": true,
      "focus": false,
      "disabled": false,
      "cols": 1,
      "rows": 1
    },
    "id": "formly_6_textarea_Textarea_1",
    "hooks": {},
    "modelOptions": {},
    "wrappers": [
      "form-field"
    ],
    "_keyPath": {
      "key": "Textarea",
      "path": [
        "Textarea"
      ]
    }
  },
  {
    "key": "Checkbox",
    "type": "checkbox",
    "templateOptions": {
      "label": "Accept terms",
      "description": "In order to proceed, please accept terms",
      "pattern": "true",
      "required": true,
      "placeholder": "",
      "focus": false,
      "disabled": false,
      "hideFieldUnderline": true,
      "indeterminate": true,
      "floatLabel": "always",
      "hideLabel": true,
      "align": "start",
      "color": "accent"
    },
    "validation": {
      "messages": {
        "pattern": "Please accept the terms"
      }
    },
    "id": "formly_6_checkbox_Checkbox_2",
    "hooks": {},
    "modelOptions": {},
    "wrappers": [
      "form-field"
    ],
    "_keyPath": {
      "key": "Checkbox",
      "path": [
        "Checkbox"
      ]
    }
  },
  {
    "key": "Radio",
    "type": "radio",
    "templateOptions": {
      "label": "Radio",
      "placeholder": "Placeholder",
      "description": "Description",
      "required": true,
      "options": [
        {
          "value": 1,
          "label": "Option 1"
        },
        {
          "value": 2,
          "label": "Option 2"
        },
        {
          "value": 3,
          "label": "Option 3"
        },
        {
          "value": 4,
          "label": "Option 4",
          "disabled": true
        }
      ],
      "focus": false,
      "disabled": false,
      "hideFieldUnderline": true,
      "floatLabel": "always",
      "tabindex": -1,
      "_flatOptions": true
    },
    "id": "formly_6_radio_Radio_3",
    "hooks": {},
    "modelOptions": {},
    "wrappers": [
      "form-field"
    ],
    "_keyPath": {
      "key": "Radio",
      "path": [
        "Radio"
      ]
    }
  },
  {
    "key": "Select",
    "type": "select",
    "templateOptions": {
      "label": "Select",
      "placeholder": "Placeholder",
      "description": "Description",
      "required": true,
      "options": [
        {
          "value": 1,
          "label": "Option 1"
        },
        {
          "value": 2,
          "label": "Option 2"
        },
        {
          "value": 3,
          "label": "Option 3"
        },
        {
          "value": 4,
          "label": "Option 4",
          "disabled": true
        }
      ],
      "focus": false,
      "disabled": false,
      "_flatOptions": true
    },
    "id": "formly_6_select_Select_4",
    "hooks": {},
    "modelOptions": {},
    "wrappers": [
      "form-field"
    ],
    "_keyPath": {
      "key": "Select",
      "path": [
        "Select"
      ]
    }
  },
  {
    "key": "select_multi",
    "type": "select",
    "templateOptions": {
      "label": "Select Multiple",
      "placeholder": "Placeholder",
      "description": "Description",
      "required": true,
      "multiple": true,
      "selectAllOption": "Select All",
      "options": [
        {
          "value": 1,
          "label": "Option 1"
        },
        {
          "value": 2,
          "label": "Option 2"
        },
        {
          "value": 3,
          "label": "Option 3"
        },
        {
          "value": 4,
          "label": "Option 4",
          "disabled": true
        }
      ],
      "focus": false,
      "disabled": false,
      "_flatOptions": true
    },
    "id": "formly_6_select_select_multi_5",
    "hooks": {},
    "modelOptions": {},
    "wrappers": [
      "form-field"
    ],
    "_keyPath": {
      "key": "select_multi",
      "path": [
        "select_multi"
      ]
    }
  }
]
