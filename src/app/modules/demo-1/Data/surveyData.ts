import { FormItemModel } from '../model/formBuilder.model';
export const surveyFormData: FormItemModel[] = [
  {
     "label":"Full Name",
     "placeholder": "Your Name",
     "name":"fullName",
     "type":"text",
     "controlWidth" : 1,
     "validation": {
       "required": true,
     }
  },
  {
    "label":"Position",
    "name":"position",
    "type":"select",
    "controlWidth" : 1,
    "validation": {
      "required": true,
    },
    "options":[
       {
          "name":"HR",
          "value":"hr"
       },
       {
         "name":"Senior Developer",
         "value":"seniorDeveloper"
       },
       {
         "name":"Technical Manager",
         "value":"technicalManager"
       },
       {
          "name":"CEO",
          "value":"ceo"
       }
    ]
 },
  {
    "label":"Email",
    "placeholder": "Your Email",
    "name":"email",
    "type":"email",
    "validation": {
     "type": "email"
   }
 },
  {
    "label":"Message",
    "name":"message",
    "type":"textArea",
    "placeholder": "Tell me more...",
    "validation": {
      "min": 3,
    }
  },
  {
     "label":"How do you describe this demo ?",
     "name":"evaluation",
     "type":"radio",
     "controlWidth" : 2,
     "validation": {
      "required": true,
      },
     "options":[
        {
           "name":"Bad",
           "value":"bad"
        },
        {
           "name":"Mediocre",
           "value":"mediocre"
        },
        {
          "name":"Good",
          "value":"good"
       },
       {
          "name":"Awesome!",
          "value":"awesome"
       }
     ]
  }
]
