import { SharedModule } from './../../shared/shared.module';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { ValidationModule } from './../../shared/validation/validation.module';
import { MaterialModule } from './../../shared/angular-material.module';
import { DemoOneRoutingModule } from './demoOne-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DemoOnePageComponent } from './views/demo-one-page/demo-one-page.component';
import { AngularSplitModule } from 'angular-split';
import { AceEditorModule } from 'ng2-ace-editor';
import { FormBuilderComponent } from './components/form-builder/form-builder.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BarRatingModule } from "ngx-bar-rating";
import { SurveyPageComponent } from './views/survey-page/survey-page.component';
import { JsonSchemaComponent } from './components/json-schema/json-schema.component';



@NgModule({
  declarations: [DemoOnePageComponent, FormBuilderComponent, SurveyPageComponent, JsonSchemaComponent],
  imports: [
    CommonModule,
    DemoOneRoutingModule,

    SharedModule,
    
    FormsModule,
    MaterialModule,
    ValidationModule,
    BarRatingModule
  ]
})
export class DemoOneModule { }
