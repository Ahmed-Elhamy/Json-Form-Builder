import { MaterialModule } from './../../shared/angular-material.module';
import { SharedModule } from './../../shared/shared.module';
import { DemoTwoPageComponent } from './../demo-2/views/demo-two-page/demo-two-page.component';
import { DemoTwoRoutingModule } from './demoTwo-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormlyMaterialModule } from '@ngx-formly/material';
import { FormlyModule } from '@ngx-formly/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularSplitModule } from 'angular-split';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { AceEditorModule } from 'ng2-ace-editor';


@NgModule({
  declarations: [DemoTwoPageComponent],
  imports: [
    CommonModule,
    DemoTwoRoutingModule,
    SharedModule,
    FormlyModule.forRoot({ extras: { lazyRender: true } }),
    FormlyMaterialModule,
    MaterialModule
    
  ]
})
export class DemoTwoModule { }
