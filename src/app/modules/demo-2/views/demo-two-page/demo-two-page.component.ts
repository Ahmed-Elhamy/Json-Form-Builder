import { formlyFakeFormData } from './../../../demo-1/Data/formlyData';
import { isMobile } from 'src/app/shared/helpers/is-mobile.helper';
import { JsonSchemaComponent } from './../../../demo-1/components/json-schema/json-schema.component';
import { FormModel } from './../../../demo-1/model/formBuilder.model';
import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-demo-two-page',
  templateUrl: './demo-two-page.component.html',
  styleUrls: ['./demo-two-page.component.scss']
})
export class DemoTwoPageComponent implements OnInit {

  public aceEditorOptions: any = {
    enableBasicAutocompletion: true,
    // maxLines: 1000,
    printMargin: false
  }
  public form = new FormGroup({});
  public model = { email: 'ahmed.ashraf@msn.com' };
  public fields = formlyFakeFormData

  public formResponse;
  public JsonValidation:string;
  public isMobile = isMobile();


  constructor(
    private _matDialog : MatDialog,
    private _snackBar: MatSnackBar,
  ) { }

  ngOnInit(): void {
  }

  public formReset(){
    this.form.reset()
    this._snackBar.open('Form has been reset successfully', 'close', {
      duration: 2000,
    });
  }

  public onSubmit() {
    this.formResponse = JSON.stringify(this.model);
    this._snackBar.open('Form has been submitted successfully', 'close', {
      duration: 2000,
    }).afterDismissed().subscribe(()=>this.form.reset())
  }


  get jsonCode () {
    return JSON.stringify(this.fields, null, 2);
  }

  set jsonCode (v) {
    try{
      this.JsonValidation = null;
      this.fields = JSON.parse(v);
    }
    catch(e) {
      this.JsonValidation = e
    };
  }
  public openJsonSchemaModal(schema) {
    this._matDialog.open(JsonSchemaComponent, {
      data: schema,
      panelClass: ['customDialog']
    })
  }

}
