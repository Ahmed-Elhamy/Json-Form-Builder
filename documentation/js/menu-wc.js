'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">JSON</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-658a3b8a9e3238a7d555d3cc28f1394e"' : 'data-target="#xs-components-links-module-AppModule-658a3b8a9e3238a7d555d3cc28f1394e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-658a3b8a9e3238a7d555d3cc28f1394e"' :
                                            'id="xs-components-links-module-AppModule-658a3b8a9e3238a7d555d3cc28f1394e"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link">AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/DemoOneModule.html" data-type="entity-link">DemoOneModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-DemoOneModule-e1905d03260e15a774b51e905650321b"' : 'data-target="#xs-components-links-module-DemoOneModule-e1905d03260e15a774b51e905650321b"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-DemoOneModule-e1905d03260e15a774b51e905650321b"' :
                                            'id="xs-components-links-module-DemoOneModule-e1905d03260e15a774b51e905650321b"' }>
                                            <li class="link">
                                                <a href="components/DemoOnePageComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DemoOnePageComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FormBuilderComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FormBuilderComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/JsonSchemaComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">JsonSchemaComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SurveyPageComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SurveyPageComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/DemoOneRoutingModule.html" data-type="entity-link">DemoOneRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/DemoTwoModule.html" data-type="entity-link">DemoTwoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-DemoTwoModule-e978471407c490b21682f73b53ebaa7a"' : 'data-target="#xs-components-links-module-DemoTwoModule-e978471407c490b21682f73b53ebaa7a"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-DemoTwoModule-e978471407c490b21682f73b53ebaa7a"' :
                                            'id="xs-components-links-module-DemoTwoModule-e978471407c490b21682f73b53ebaa7a"' }>
                                            <li class="link">
                                                <a href="components/DemoTwoPageComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DemoTwoPageComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/DemoTwoRoutingModule.html" data-type="entity-link">DemoTwoRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/LayoutModule.html" data-type="entity-link">LayoutModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-LayoutModule-42ee4cf498d8868caddfa35526102db3"' : 'data-target="#xs-components-links-module-LayoutModule-42ee4cf498d8868caddfa35526102db3"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-LayoutModule-42ee4cf498d8868caddfa35526102db3"' :
                                            'id="xs-components-links-module-LayoutModule-42ee4cf498d8868caddfa35526102db3"' }>
                                            <li class="link">
                                                <a href="components/AuthorizedLayoutComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AuthorizedLayoutComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NavBarComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NavBarComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/MaterialModule.html" data-type="entity-link">MaterialModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/PrimeNGModule.html" data-type="entity-link">PrimeNGModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/SharedModule.html" data-type="entity-link">SharedModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/TranslocoRootModule.html" data-type="entity-link">TranslocoRootModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ValidationModule.html" data-type="entity-link">ValidationModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ValidationModule-dfe9587d9de89a5df56b1301b4313582"' : 'data-target="#xs-components-links-module-ValidationModule-dfe9587d9de89a5df56b1301b4313582"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ValidationModule-dfe9587d9de89a5df56b1301b4313582"' :
                                            'id="xs-components-links-module-ValidationModule-dfe9587d9de89a5df56b1301b4313582"' }>
                                            <li class="link">
                                                <a href="components/ValidationErrorComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ValidationErrorComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/LayoutService.html" data-type="entity-link">LayoutService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TranslocoHttpLoader.html" data-type="entity-link">TranslocoHttpLoader</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ValidationService.html" data-type="entity-link">ValidationService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/FormDataModel.html" data-type="entity-link">FormDataModel</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/FormItemModel.html" data-type="entity-link">FormItemModel</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/FormModel.html" data-type="entity-link">FormModel</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/MultiOptionModel.html" data-type="entity-link">MultiOptionModel</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ratingModel.html" data-type="entity-link">ratingModel</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});