# Json Form Builder
**A project that can build dynamic forms out of a JSON object.**

---

# Find a [Demo](https://json-form-builder.herokuapp.com) here
## Description:
Design and develop front-end that can render HTML forms dynamically based on provided JSON Objects.
One use case of this is to be able to store Form definitions into Database in JSON format and then dynamically render different forms to different clients based on different factors, e.g: Client permissions.
However, the scope of this challenge is to just build the renderer part on the UI with a text area to drop-in the JSON Object.

## UI:
Only one screen.
A place to paste/edit the JSON Object
A place to render the resulted HTML form

## Functionality:
Render HTML forms based on provided JSON Object
Supported inputs can be ( Label, Textbox, Dropdown, Checkbox, RadioButton, Submit, and Cancel Button  )
Once the user pastes or updates the JSON object in the text-area, that should be reflected automatically on the rendered form.

## General Requirements:
It Can be implemented using Vanilla JS and HTML but implementing in Angular is a plus.
You can use any external libraries to help you.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

Run `npm run start` for a local dev server using express. Navigate to `http://localhost:8080/`.
## Further help for developers
A well documentation is provided, please read the [Documentation](documentation/)
